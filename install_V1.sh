#!/bin/bash
# -*- bash -*-

# Takuan/GAmera installation and update script for Ubuntu/Debian
# and openSUSE environments
# Copyright (C) 2009-2010 Antonio García Domínguez
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# This script simply automates all the steps included in the Takuan/GAmera
# installation instructions. It is a poor man's substitute to a properly
# written set of Ubuntu/Debian/openSUSE packages.

# Exit on error
set -e

### USER CONFIGURATION ########################################################

# Path under which all downloads should be cached to save bandwidth
DL_CACHE="${HOME}/downloads"

# Path under which all binaries will be installed
BIN_PATH="${HOME}/bin"

# Path under which all the source code will be unpacked
SRC_PATH="${HOME}/src"

# Path to symlink under which Tomcat will be accesible (keep the BIN_PATH part)
export CATALINA_HOME="${BIN_PATH}/tomcat5"

# Path to where BPELUnit will be installed
export BPELUNIT_HOME="${BIN_PATH}/bpelunit"

# Path from which XMLBeans will be accesible
export XMLBEANS_HOME="${BIN_PATH}/xmlbeans"

# Path under which Saxon 9 will be unpacked
SAXON_HOME="${BIN_PATH}/saxon9"

# Path under which CM3 will be installed
CM3_HOME="/usr/local/cm3"

# Path under which Takuan will be installed
TAKUAN_HOME="${HOME}/takuan"

# Path under which GAmera (and Koura) will be installed
GAMERA_HOME="${HOME}/GAmera"

### SCRIPT CONFIGURATION ######################################################

# Base URL to use as prefix for all downloads
#
# Note to developers: be sure to customize it when you copy this script to
# a tag or a branch!
BASE_SVN_URL="https://neptuno.uca.es/svn/sources-fm/branches/gamera-1.0"

# Default installation target (if any). Leave empty if you don't want
# to use any.
#
# Same note as above.
TARGET="gamera"

### you shouldn't need to change much below here, unless you've changed ###
### the layout of the Subversion repository or its dependencies.        ###

# Tomcat 5.5 (binary dist)
TOMCAT_BIN_SVNURL="deps/apache-tomcat-5.5.26.zip"

# Main directory in the Tomcat 5.5 binary distribution
TOMCAT_BIN_MAINDIR="apache-tomcat-5.5.26"

# ActiveBPEL 4.1 (source dist)
ACTIVEBPEL_SRC_SVNURL="deps/activebpel-4.1-src.tar.gz"

# ActiveBPEL 4.1 patches for Takuan
ACTIVEBPEL_DELTA_SVNURL="src/parches/ActiveBPEL_delta_4.1.diff.gz"

# ActiveBPEL 4.1 (launcher)
ACTIVEBPEL_LAUNCHER_SVNURL="scripts/ActiveBPEL.sh"

# ActiveBPEL 4.1 launcher basename
ACTIVEBPEL_LAUNCHER_BASENAME=`basename "$ACTIVEBPEL_LAUNCHER_SVNURL"`

# Main directory in the ActiveBPEL 4.1 source distribution
ACTIVEBPEL_SRC_MAINDIR="activebpel-4.1"

# Deployment directory for ActiveBPEL
ACTIVEBPEL_DEPLOY_DIR="${CATALINA_HOME}/bpr"

# Path in SVN to the source of the UCA XPath monitoring extensions
XPATHEXT_SRC_SVNURL="src/ExtensionesXPathUCA"

# Path to the .jar with the UCA XPath monitoring extensions
XPATHEXT_JAR_PATH="${CATALINA_HOME}/shared/lib/xpath_uca.jar"

# Name of the class to be used to test the correct installation of the
# XPath monitoring extensions
XPATHEXT_TEST_CLASS=es.uca.webservices.xpath.StringXPathExpression

# XMLBeans 2.1.0 (binary dist)
XMLBEANS_BIN_SVNURL="deps/xmlbeans-2.1.0.tgz"

# Main directory in the XMLBeans 2.1.0 binary distribution
XMLBEANS_BIN_MAINDIR="xmlbeans-2.1.0"

# BPELUnit (path to our svn:external)
BPELUNIT_SRC_SVNURL="deps/BPELUnit"

# Path to configuration.xml inside build/install directory
BPELUNIT_CONF="conf/configuration.xml"

# Saxon-J 9.1.0.1 (binary dist)
SAXON_BIN_SVNURL="deps/saxonb9-1-0-1j.zip"

# Paths which will be added to the Java classpath for Saxon
SAXON_CLASSPATH="${SAXON_HOME}/saxon9.jar:${SAXON_HOME}/saxon9-dom.jar"

# JUnit 4 (binary dist)
JUNIT4_BIN_SVNURL="deps/junit4.jar"

# CM3 minimal bootstrap binary distribution, 32 bits
CM3MIN_BIN_SVNURL="deps/cm3-min-POSIX-LINUXLIBC6-d5.6.0-2008-02-23-03-35-01.tgz"

# CM3 full source distribution, 32 bits
CM3_SRC_SVNURL="deps/cm3-src-all-d5.6.0-2008-03-04-01-01-39.tgz"

# Unofficial CM3 .deb for 64 bits
CM3_DEB64_SVNURL="deps/cm3-std-AMD64_LINUX-d5.8.1.deb"

# Path under which the CM3 minimal distribution installer will be unpacked
CM3MIN_UNPACK_PATH="${BIN_PATH}/cm3-min-install"

# Path under which the CM3 source will be unpacked
CM3_SRC_UNPACK_PATH="${SRC_PATH}/cm3-5.6.0"

# Simplify source distribution
SIMPLIFY_SRC_SVNURL="deps/simplify-SVN71394.tar.bz2"

# Main directory in the Simplify source distribution
SIMPLIFY_SRC_MAINDIR="simplify"

# Daikon source distribution
DAIKON_SRC_SVNURL="deps/daikon-4.3.4.tar.gz"

# Daikon launcher script
DAIKON_SH_SVNURL="scripts/daikon.sh"

# Daikon options
DAIKON_OPTIONS_SVNURL="scripts/daikon.options"

# Modifications to Daikon
DAIKON_DELTA_SVNURL="src/parches/Daikon_delta_4.3.4.diff.gz"

# Main directory in the Daikon source distribution
DAIKON_SRC_MAINDIR="daikon"

# Classpath which will be used in the Daikon script
DAIKON_CLASSPATH="${SRC_PATH}/${DAIKON_SRC_MAINDIR}/java"

# Subfolders from SVN to be checked out at TAKUAN_HOME
TAKUAN_SVNURLS="samples src"

# Folder with the instrumenter's Ant buildfile
TAKUAN_INSTR_SRC_PATH="${TAKUAN_HOME}/src/InstrumentadorProcesoBPEL"

# Sample to be run to verify that Takuan works
TAKUAN_SAMPLE_PATH="${TAKUAN_HOME}/samples/LoanApprovalRPC"

# Path to the Takuan servlet WAR
TAKUAN_SERVLET_WAR="${CATALINA_HOME}/webapps/takuan.war"

# Folder with the servlet's Ant buildfile
TAKUAN_SERVLET_SRC="${TAKUAN_HOME}/src/Takuan_Server"

# Basename of the servlet's Ant buildfile
TAKUAN_SERVLET_ANT="standalone-build.xml"

# mutationtool launcher script
MUTATIONTOOL_SCRIPT_SVNURL="bin/mutationtool"

# mutationtool JAR file
MUTATIONTOOL_JAR_SVNURL="bin/mutation_analysis.jar"

# galib source distribution
GALIB_SOURCETGZ_SVNURL="deps/galib247.tgz"

# galib source main diirectory
GALIB_SOURCE_MAINDIR="galib247"

# Path in SVN to latest source of GAmera
GAMERA_SOURCE_SVNURL="src/GAmera"

# Basename of the main executable (GAmera)
GAMERA_EXE="gamera"

# Path in SVN to the Koura source
KOURA_SOURCE_SVNURL="src/Koura/Mutantes/Mutantes"

# Basename of the main executable (Koura)
KOURA_EXE="Mutantes.exe"

# Configuration to be compiled: "Debug" by default.
# Should be the same as in the active configuration in Koura's .mds, as mdtool
# in openSUSE 11.0's monodevelop package does not support the -c flag.
KOURA_CONFIGURATION="Debug"

# Basename of the Koura launcher
KOURA_LAUNCHER_BASENAME="koura"

# Extra dependencies for Koura
KOURA_EXTRADEPS_SVNURLS="src/Koura/img src/Koura/xmldiff.pl src/Koura/xmlpp.pl"

### AUXILIARY FUNCTIONS #######################################################

function echo_err() {
  echo "$@" >&2
}

function log() {
  if [[ "$#" < 2 ]]; then
    error "Wrong parameters: expected level and rest of message"
    return 1
  fi
  LEVEL="$1"; shift
  echo_err "`date -R`: ($LEVEL) $@"
}

function status() {
  log INFO "$@"
}

function error() {
  log ERROR "$@"
}

function warn() {
  log WARN "$@"
}

function push_d() {
  # Create if it doesn't exist
  if test -f "$1"; then
    error "$1 is a file"
    return 1
  fi
  mkdir -p "$1"
  pushd "$1" > /dev/null
}

function pop_d() {
  popd > /dev/null
}

# Installs several Debian packages, without asking for confirmation.  Relies on
# several utilities found in recent Debian-based distros.  Produces an error if
# they are not present.
#
# Accepts a mix of package names and filenames, like this:
#
# install_deb_packages emacs my-other-local-file.deb
#
# Every argument which matches a file in the local filesystem will be passed to
# gdebi, and the rest will be passed to aptitude.
function install_deb_packages() {
  declare -a FILES
  declare -a NAMES
  for f in $@; do
    if test -f "$f"; then
      FILES[${#FILES[*]}]=$f
    else
      NAMES[${#NAMES[*]}]=$f
    fi
  done
  if test "${NAMES[*]}" != ""; then
    sudo aptitude install -yq2 ${NAMES[@]}
  fi
  if test "${FILES[*]}" != ""; then
    sudo gdebi -n ${FILES[@]}
  fi
}

# Installs several RPM packages, without asking for confirmation.  Relies on the
# Zypper tool used by recent openSUSE releases. Produces an error if it is not
# present.
#
# Accepts anything Zypper accepts, which is a mix of package names and
# filenames:
#
# install_rpm_packages emacs my-local-package.rpm
function install_rpm_packages() {
  sudo zypper -n install -l $@
}

# Installs several Perl modules from CPAN, without asking for confirmation.
function install_from_cpan() {
  yes | sudo cpan $@
}

function has_perl_module() {
  if [ "$#" != 1 ]; then
     error "Wrong parameters: expected Perl module name (such as Module::Package)."
     return 1
  fi
  perl -M"$1" -e 1 >/dev/null
}

function download_from_svn() {
  if [ "$#" != 1 ]; then
    error "Wrong parameters: downloadFromSVN expects relative url from $BASE_SVN_URL."
    return 1
  fi
  URL="$BASE_SVN_URL/$1"
  BASENAME=`basename "$URL"`
  CACHE_PATH=$DL_CACHE/$BASENAME

  if ! has_perl_module "Date::Parse"; then
    install_package libtimedate-perl
  fi

  if test -e "$CACHE_PATH"; then
    # Floating-point math doesn't work well from Bash scripts, and stat(1) returns
    # an integer anyway, so we truncate the SVN timestamp to seconds. We do lose
    # some accuracy, but it'd only be a problem if we did several commits in less
    # than a second, and we somehow exported the file in one of the revisions
    # in-between, which isn't all too likely.
    CACHED_DATE_SECS=`stat -c "%Y" "$CACHE_PATH"`
    SVN_DATE=`svn info --xml "$URL" | sed -nre 's#<date>(.*)</date>#\1#p'`
    SVN_DATE_SECS=`perl -MDate::Parse -e "print int(str2time('$SVN_DATE'));"`
    if (( $SVN_DATE_SECS <= $CACHED_DATE_SECS )); then
      status "Cached copy of $BASENAME in $DL_CACHE is up to date."
    else
      status "Cached copy of $BASENAME in $DL_CACHE is outdated (SVN: $SVN_DATE_SECS, cached: $CACHED_DATE_SECS)."
      rm -rf "$CACHE_PATH"
    fi
  fi

  if test ! -e "$CACHE_PATH"; then
    status "Downloading $URL into $CACHE_PATH..."
    push_d "$DL_CACHE"
    svn export -q "$URL"
    pop_d
  fi

  echo $CACHE_PATH
}

function checkout_from_svn() {
  if [ "$#" != 1 -a "$#" != 2 ]; then
    error "Wrong parameters: checkout_from_svn expected relative path to folder from $BASE_SVN_URL, and optionally the checkout path."
    return 1
  fi

  SVNPATH="$1"
  URL="$BASE_SVN_URL/$SVNPATH"
  FLAGS=""
  if [ "$#" == 2 ]; then
    DEST="$2"
  else
    DEST=`pwd`/`basename "$URL"`
  fi

  if test -e "$DEST"; then
    status "The SVN checkout path $DEST already exists. I need to remove it to continue."
    if ! confirm_rm "$DEST"; then
      return 1
    fi
  fi

  if ! svn info "$URL" 2>/dev/null | grep -q .; then
      # If it doesn't exist as a regular path in the SVN repository
      # (svn info doesn't produce anything on stdout), then it might
      # be an svn:external reference. Parse the URL from the
      # svn:externals property and check that out.

      # Escape slashes in the path
      ESC_SVNPATH=`sed 's#/#\\\\/#g' <<<"$SVNPATH"`

      # Do the parsing. NOTE: this will not work with the new formats
      # in SVN 1.5 and 1.6!
      SVN_EXT_LINE=`svn pg svn:externals "$BASE_SVN_URL" |\
        gawk -F'[ \t]+' "/^$ESC_SVNPATH[[:space:]]+/ {print \\$2 \",\" \\$3}"`

      if [ -z "${SVN_EXT_LINE%,*}" ]; then
          echo "$SVNPATH isn't external and doesn't exist at $BASE_SVN_URL."
          return 2
      elif [ -n "${SVN_EXT_LINE#*,}" ]; then
          FLAGS=${SVN_EXT_LINE%,*}
          URL=${SVN_EXT_LINE#*,}
      else
          URL=${SVN_EXT_LINE%,*}
      fi
  fi

  svn checkout $FLAGS "$URL" "$DEST"
  echo "$DEST"
}

function confirm() {
  RESULT=""
  while test "$RESULT" != "y" -a "$RESULT" != "n" ; do
    echo_err -n "$1 [yn] "
    read RESULT
    RESULT=`tr [:upper:] [:lower:] <<<"$RESULT"`
  done
  test "$RESULT" = "y"
}

# Checks whether an environment variable has been set somewhere in the
# user's profile file.
function has_env_var() {
  if test "$#" != 1; then
    error "Wrong parameters: expected variable name"
    return 1
  fi

  VAR="$1"
  egrep -q "^export +$VAR=" ~/.profile
}

# Adds a specific line to the user's .profile file
function add_profile_line() {
  if test "$#" != 1; then
    error "Wrong parameters: expected line to be added to ~/.profile"
    return 1
  fi

  # We need to keep in sync with the .profile file
  LINE="$1"
  eval $LINE
  if ! grep -q "$LINE" ~/.profile; then
    echo "$LINE" >>~/.profile
    status "Added line '$LINE' to ~/.profile"
  else
    warn "Line '$LINE' was already in ~/.profile. Will not do anything."
  fi
}

# Saves an environment variable to the user's .profile. Doesn't modify any
# prior bindings to the same environment variable in the file.
function save_env_var() {
  if test "$#" != 2; then
    error "Wrong parameters: expected variable name and value"
    return 1
  fi

  VAR="$1"
  VALUE="$2"
  LINE="export $VAR=$VALUE"

  add_profile_line "$LINE"
}

# Removes _all_ assignments to an environment variable from the user's .profile.
# The old .profile is saved in .profile.bak.
function clear_env_var() {
  if test "$#" != 1; then
    error "Wrong parameters: expected variable name"
    return 1
  fi

  VAR="$1"
  sed -i.bak -e "/^export $VAR=/d" ~/.profile

  # Check that the variable is not bound anymore
  if ! has_env_var "$VAR"; then
    status "Removed all bindings for $VAR from ~/.profile"
  else
    error "Could not remove bindings for $VAR from ~/.profile"
    return 1
  fi
}

function has_java_version() {
  if test "$#" != 1; then
    error "Wrong arguments: expected Java version (5, 6, etc.)."
    return 1
  fi

  EXPECTED="$1"
  OBTAINED=`java -version 2>&1 | sed -nre '1{s/.*"([^_]+).*"/\1/gp}'`
  if test -n "$OBTAINED"; then
    status "Looking for Java $EXPECTED, found Java $OBTAINED set as default"
  else
    status "Looking for Java $EXPECTED, didn't find any JRE set up"
  fi
  test "$EXPECTED" = "$OBTAINED"
}

function can_run() {
  if test "$#" != 1; then
    error "Wrong arguments: expected command name."
    exit 1
  fi

  CMD="$1"
  type -P "$CMD" >/dev/null
}

function install_script() {
  if test "$#" != 1; then
    error "Wrong arguments: expected path to script."
    return 1
  fi

  SCRIPT=$1
  BASENAME=`basename "$SCRIPT"`
  DEST=$BIN_PATH/$BASENAME
  cp "$SCRIPT" "$DEST"
  chmod +x "$DEST"
  if ! type -P "$BASENAME" >/dev/null; then
    # Need to add $BIN_PATH to PATH
    export PATH="$PATH":"$BIN_PATH"
    save_env_var PATH "\$PATH:$BIN_PATH"
    status "Added $BIN_PATH to the PATH environment variable"
  fi

  status "Installed $SCRIPT in $BIN_PATH"
}

function zpatch() {
  if test "$#" != 1; then
    error "Wrong arguments: expected path to gzipped patch."
    return 1
  fi

  PATCH="$1"
  set +e
  zcat "$PATCH" | patch -p1
  if (($? <= 1)); then
    set -e
    return 0
  else
    set -e
    return 1
  fi
}

function unpack() {
  if test "$#" != 1; then
    error "Wrong arguments: expected path to compressed archive."
    return 1
  fi

  ARCHIVE="$1"
  if ! test -e "$ARCHIVE"; then
    error "Cannot unpack $ARCHIVE: does not exist"
    return 2
  elif ! test -f "$ARCHIVE"; then
    error "Cannot unpack $ARCHIVE: it's not a file"
    return 3
  fi

  MIME=`file --mime-type "$ARCHIVE" | cut -d' ' -f2`
  case "$MIME" in
    application/*gzip)
      tar xzf "$ARCHIVE"
      ;;
    application/*bzip2)
      tar xjf "$ARCHIVE"
      ;;
    application/zip)
      unzip "$ARCHIVE"
      ;;
    *)
      error "Unknown archive format in $ARCHIVE: MIME type is $MIME"
      return 1
      ;;
  esac
}

function mcore_make() {
  CORES=1
  if grep -q "cpu cores" /proc/cpuinfo; then
    CORES=`grep "cpu cores" /proc/cpuinfo | head -1 | sed -nre 's/cpu cores\s+:\s+([0-9]+)/\1/gp'`
  fi
  make -j"$CORES" $@
}

function host_is_amd64() {
  ARCH=`uname -m`
  if [ "$ARCH" = 'x86_64' ]; then
    status "$ARCH: 64 bits, x86"
    return 0
  else
    status "$ARCH: unknown architecture, assuming 32 bits x86"
    return 1
  fi
}

function has_library() {
    if test "$#" != 1; then
        error "Wrong arguments: expected library name (e.g. 'm' for libm)"
        return 1
    fi
    LIBRARY="$1"
    if ld -l"$LIBRARY" &>/dev/null; then
        status "Library $LIBRARY is available"
        return 0
    else
        warn "Library $LIBRARY is not available"
        return 1
    fi
}

# Substitute for "rm -I" (coreutils 7.4). "rm -I" doesn't tell the user what
# files are going to be removed before asking for confirmation. It just
# says something like "remove all arguments?", which is useless if we're
# calling it from a shell script, like in our case.
#
# This function tells the user what is going to be removed and asks for
# confirmation. If confirmed, the files are removed and a zero status code
# is returned. Otherwise, a non-zero status code is returned. Only existing
# paths are reported to the user: if none of them exist, this function will
# simply do nothing.
#
# This function accepts a sequence of paths to be removed. Optionally, the
# -s may be used before any path, to indicate sudo should be used to remove
# the files with root privileges.
#
# Suggested tests:
# - args = []                         -> error about usage
# - args = [a], !exist(a)             -> nothing
# - args = [a], exist(a)              -> prompt([a]),   confirm -> !exist(a)
# - args = [a], exist(a)              -> prompt([a]),   cancel  -> status(1), exist(a)
# - args = [a,b], !exist(a), !exist(b)-> nothing
# - args = [a,b], !exist(a), exist(b) -> prompt([b]),   confirm -> !exist(a),!exist(b)
# - args = [a,b], exist(a), !exist(b) -> prompt([a]),   confirm -> !exist(a),!exist(b)
# - args = [a,b], exist(a), exist(b)  -> prompt([a,b]), confirm -> !exist(a),!exist(b)
# - args = [a,b], exist(a), exist(b)  -> prompt([a,b]), confirm -> !exist(a),!exist(b)
# - args = [a,b], exist(a), exist(b)  -> prompt([a,b]), cancel  -> status(1), exist(a), exist(b)
function confirm_rm() {
  if test "$#" == 0; then
    error "Usage: [-s] path(s)..."
    error "   -s: use root privileges (sudo) for removing the paths"
    exit 1
  fi

  # -s: use sudo to run rm
  OPTIND=1
  MSG_TEXT="All files under the following paths will be removed with the rights of user"
  if getopts :s OPCIONES; then
      MSG_TEXT="$MSG_TEXT 'root':"
      CMD_PREFIX="sudo "
  else
      MSG_TEXT="$MSG_TEXT '`whoami`':"
      CMD_PREFIX=""
  fi

  SOME_FILE_EXISTS=0
  for f in $@; do
    if test -e "$f"; then
      if test "$SOME_FILE_EXISTS" == 0; then
        echo "$MSG_TEXT"
        SOME_FILE_EXISTS=1
      fi
      echo "  - $f"
    fi
  done
  if test "$SOME_FILE_EXISTS" == 1; then
    if confirm "Are you sure?"; then
      $CMD_PREFIX rm -rf -- $@
    else
      return 1
    fi
  fi
}

### COMMON DEPENDENCIES ########################################################

function prepare_repositories() {
  if can_run aptitude; then
    sudo aptitude update
  else
    sudo zypper refresh
  fi
}

function install_base_deps() {
  if can_run aptitude; then
    install_deb_packages subversion unzip curl ant ant-optional patch gdebi \
        sed gawk
  else
    install_rpm_packages subversion unzip curl ant ant-junit ant-trax patch \
        sed gawk
  fi
}

function install_java6_jdk() {
  if has_java_version 1.6.0 && has_env_var JAVA_HOME; then
    if ! confirm "A Java 6 SE JDK seems to be already installed and set up. Reinstall?"; then
      return 0
    fi
    clear_env_var JAVA_HOME
    clear_env_var JDK_HOME
    clear_env_var JDKDIR
  fi

  status "Installing OpenJDK 6"
  if can_run aptitude; then
    install_deb_packages openjdk-6-jdk
    save_env_var JAVA_HOME /usr/lib/jvm/java-6-openjdk
    sudo update-java-alternatives --set java-6-openjdk
  else
    install_rpm_packages java-1_6_0-openjdk-devel
    save_env_var JAVA_HOME /usr/lib/jvm/java-1.6.0-openjdk

    # openSUSE 11.0: OpenJDK 6 might already be installed, but some other JDK
    # might be set as the default. We will take advantage of the fact that
    # --auto sets the alternative to that with the highest priority (OpenJDK 6
    # has higher priority than Sun JDK 6 and IcedTea 7), seeing that the
    # version of update-alternatives shipped in openSUSE 11.0 does not have
    # the --set option.
    sudo /usr/sbin/update-alternatives --auto java
    sudo /usr/sbin/update-alternatives --auto javac
  fi
  save_env_var JDK_HOME  \$JAVA_HOME
  save_env_var JDKDIR    \$JDK_HOME
  save_env_var CLASSPATH \$CLASSPATH:\$JDKDIR/lib/tools.jar
  save_env_var CLASSPATH \$CLASSPATH:.

  if has_java_version 1.6.0 && has_env_var JAVA_HOME; then
    status "Successfully installed and set up OpenJDK 6."
  else
    error "Failed to install and set up OpenJDK 6."
    return 1
  fi
}

function install_tomcat5() {
  if test -d "$CATALINA_HOME"; then
    if ! (confirm "Tomcat 5.5 seems to be already installed at $CATALINA_HOME. Reinstall?" &&\
          confirm_rm "$CATALINA_HOME" "$BIN_PATH/$TOMCAT_BIN_MAINDIR"); then
      return 0
    fi
    clear_env_var CATALINA_HOME
  fi

  status "Installing Tomcat 5.5..."
  TOMCAT5_GZ=`download_from_svn "$TOMCAT_BIN_SVNURL"`
  push_d "$BIN_PATH"
  unpack "$TOMCAT5_GZ"
  ln -s "$BIN_PATH/$TOMCAT_BIN_MAINDIR" "$CATALINA_HOME"
  status "Unpacked and linked Tomcat directory to $CATALINA_HOME"
  pop_d
  save_env_var CATALINA_HOME "$CATALINA_HOME"

  status "Successfully installed Tomcat 5.5."
}

function install_activebpel() {
  if test -d "$ACTIVEBPEL_DEPLOY_DIR" -o -d "$SRC_PATH/$ACTIVEBPEL_SRC_MAINDIR"; then
    if ! (confirm "ActiveBPEL seems to be already installed. Reinstall?" &&\
          confirm_rm "$SRC_PATH/$ACTIVEBPEL_SRC_MAINDIR"\
            "$ACTIVEBPEL_DEPLOY_DIR"\
            "$BIN_PATH/$ACTIVEBPEL_LAUNCHER_BASENAME"); then
      return 0
    fi
    "$ACTIVEBPEL_LAUNCHER_BASENAME" stop || true
  fi

  # openSUSE 11.0: the Xalan serializer is split apart from the
  # main Xalan JAR and needs to be added manually to the CLASSPATH
  if can_run zypper; then
    save_env_var CLASSPATH \$CLASSPATH:/usr/share/java/xalan-j2-serializer.jar
  fi

  # Install the launcher script
  ACTIVEBPEL_SH=`download_from_svn "$ACTIVEBPEL_LAUNCHER_SVNURL"`
  install_script "$ACTIVEBPEL_SH"

  # Unpack, compile and install ActiveBPEL
  ACTIVEBPEL_SRC_TGZ=`download_from_svn "$ACTIVEBPEL_SRC_SVNURL"`
  ACTIVEBPEL_DELTA=`download_from_svn "$ACTIVEBPEL_DELTA_SVNURL"`
  push_d "$SRC_PATH"
  unpack "$ACTIVEBPEL_SRC_TGZ"
  push_d "$ACTIVEBPEL_SRC_MAINDIR"
  zpatch "$ACTIVEBPEL_DELTA"
  bash recompile.sh
  pop_d
  pop_d

  if "$ACTIVEBPEL_LAUNCHER_BASENAME" start; then
    "$ACTIVEBPEL_LAUNCHER_BASENAME" stop
    status "ActiveBPEL 4.1 has been succesfully patched, compiled and installed."
    status "ActiveBPEL has been configured with full logging enabled."
  else
    error "Failed to install ActiveBPEL."
    return 1
  fi
}

function install_xpath_monitoring() {
    XPATHEXT_SRC_BASENAME=`basename "$XPATHEXT_SRC_SVNURL"`
    XPATHEXT_SRC_PATH=$SRC_PATH/$XPATHEXT_SRC_BASENAME

    if test -e "$XPATHEXT_JAR_PATH" || test -e "$XPATHEXT_SRC_PATH"; then
        if ! (confirm \
            "The UCA XPath monitoring extensions seem to be already installed. Reinstall?" && \
            confirm_rm "$XPATHEXT_JAR_PATH" "$XPATHEXT_SRC_PATH"); then
            return 0
        fi
        rm -rf "$XPATHEXT_SRC_PATH"
        rm -rf "$XPATHEXT_JAR_PATH"
    fi

    # We'll need some of the previously installed environment variables
    . ~/.profile

    # Checkout the source, compile and copy the .jar to the right place
    checkout_from_svn "$XPATHEXT_SRC_SVNURL" "$XPATHEXT_SRC_PATH"
    pushd "$XPATHEXT_SRC_PATH"
    ant dist
    cp *.jar "$XPATHEXT_JAR_PATH"
    popd

    # Make sure the .jar was put in the right place, and that it contains Java code
    if javap -classpath "$XPATHEXT_JAR_PATH" "$XPATHEXT_TEST_CLASS"; then
        status "Successfully installed the UCA XPath monitoring extensions."
    else
        error "Failed to install the UCA XPath monitoring extensions."
        return 1
    fi
}

function install_xmlbeans() {
  if test -d "$XMLBEANS_HOME"; then
    if ! (confirm "XMLBeans 2.1.0 seems to be already installed. Reinstall?" &&\
          confirm_rm "$XMLBEANS_HOME" "$BIN_PATH/$XMLBEANS_BIN_MAINDIR"); then
      return 0
    fi
    clear_env_var XMLBEANS_HOME
  fi

  XMLBEANS_BIN_TGZ=`download_from_svn "$XMLBEANS_BIN_SVNURL"`
  push_d "$BIN_PATH"
  unpack "$XMLBEANS_BIN_TGZ"
  ln -s "$BIN_PATH/$XMLBEANS_BIN_MAINDIR" "$XMLBEANS_HOME"
  pop_d
  save_env_var XMLBEANS_HOME "$XMLBEANS_HOME"

  if test -r "$XMLBEANS_HOME/lib/xbean.jar"; then
    status "Successfully installed XMLBeans 2.1.0."
  else
    error "Failed to install XMLBeans 2.1.0."
    return 1
  fi
}

function install_bpelunit() {
  BPELUNIT_SRC_PATH=$SRC_PATH/`basename "$BPELUNIT_SRC_SVNURL"`

  if test -d "$BPELUNIT_SRC_PATH" || test -d "$BPELUNIT_HOME"; then
    if ! (confirm "BPELUnit seems to be already installed. Reinstall?" &&\
          confirm_rm "$BPELUNIT_SRC_PATH" "$BPELUNIT_HOME"); then
      return 0
    fi
    clear_env_var BPELUNIT_HOME
  fi

  # Checkout the source from SVN and compile
  checkout_from_svn "$BPELUNIT_SRC_SVNURL" "$BPELUNIT_SRC_PATH"
  push_d "$BPELUNIT_SRC_PATH"/org.bpelunit.build.standalone
  ant

  # Use our own configuration.xml
  cat > "build/conf/configuration.xml" <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<testConfiguration xmlns="http://www.bpelunit.org/schema/testConfiguration"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.bpelunit.org/schema/testConfiguration">
  <configuration deployer="activebpel">
    <property name="DeploymentDirectory">$ACTIVEBPEL_DEPLOY_DIR</property>
    <property name="DeploymentAdminServiceURL">http://localhost:8080/active-bpel/services/DeployBPRService</property>
    <property name="AdministrationServiceURL">http://localhost:8080/active-bpel/services/ActiveBpelAdmin</property>
  </configuration>
</testConfiguration>
EOF
  mkdir -p "$BPELUNIT_HOME"
  cp -r build/* "$BPELUNIT_HOME"

  # Exit from the BPELUnit source
  pop_d

  # Install the launcher script
  BPELUNIT_SH=`download_from_svn scripts/bpelunit.sh`
  install_script "$BPELUNIT_SH"

  # Save the BPELUNIT_HOME environment variable
  save_env_var BPELUNIT_HOME "$BPELUNIT_HOME"

  if bpelunit.sh | grep -qv "not find"; then
    status "Successfully installed BPELUnit."
  else
    error "Failed to install BPELUnit."
    return 1
  fi
}

function install_saxon() {
  if (java net.sf.saxon.Transform &>/dev/null; (( $? == 2 ))); then
    if ! (confirm "Saxon 9.1.0.1 seems to be already installed. Reinstall?" &&\
          confirm_rm "$SAXON_HOME"); then
      return 0
    fi
    clear_env_var SAXON_HOME
  fi

  SAXON_TGZ=`download_from_svn "$SAXON_BIN_SVNURL"`
  push_d "$SAXON_HOME"
  unpack "$SAXON_TGZ"
  pop_d

  save_env_var CLASSPATH "\$CLASSPATH:$SAXON_CLASSPATH"
  save_env_var SAXON_HOME "$SAXON_HOME"

  if (java net.sf.saxon.Transform &>/dev/null; (( $? == 2 ))); then
    status "Saxon 9.1.0.1 has been successfully installed."
  else
    error "Failed to installed Saxon."
    return 1
  fi
}

### TAKUAN DEPENDENCIES ########################################################

function install_cm3() {
  if test -d "$CM3_HOME" \
          -o -d "$CM3_SRC_UNPACK_PATH" \
          -o -d "$CM3MIN_UNPACK_PATH" \
     || can_run cm3
  then
    if can_run cm3; then
      if ! (confirm "Critical Mass Modula-3 (CM3) seems to be already installed. Reinstall?" &&\
            confirm_rm -s "$CM3_HOME" "$CM3MIN_UNPACK_PATH" "$CM3_SRC_UNPACK_PATH"); then
        return 0
      fi
    else
      status "Found a broken installation of CM3: cleaning up before retrying..."
    fi
  fi

  # Install required packages
  if can_run aptitude; then
    install_deb_packages gcc g++ binutils flex \
      freeglut3-dev unixodbc-dev libmotif-dev \
      libx11-dev libxmu-dev libpq-dev \
      libxaw7-dev libgmp3-dev make
  else
    install_rpm_packages gcc gcc-c++ binutils flex \
      freeglut-devel unixODBC-devel openmotif-devel \
      xorg-x11-libX11-devel xorg-x11-libXmu-devel libpqxx-devel \
      xaw3d-devel gmp-devel make
  fi

  if host_is_amd64; then
      # FIXME: add 64bit RPM
      CM3STD_DEB=`download_from_svn "$CM3_DEB64_SVNURL"`
      install_deb_packages "$CM3STD_DEB"
      sudo chown `id -un`.`id -gn` "$CM3_HOME"
  else
      # CM3: install minimal bootstrap distribution
      sudo mkdir -p "$CM3_HOME"
      sudo chown `id -un`.`id -gn` "$CM3_HOME"
      push_d "$CM3MIN_UNPACK_PATH"
      CM3MIN_TGZ=`download_from_svn "$CM3MIN_BIN_SVNURL"`
      unpack "$CM3MIN_TGZ"
      ./cminstall
      pop_d
  fi

  # Add the CM3 libraries to the global search path
  if ! /sbin/ldconfig -v 2>&1 | grep -q /usr/local/cm3/lib; then
    echo "/usr/local/cm3/lib" | sudo tee /etc/ld.so.conf.d/cm3.conf
    sudo /sbin/ldconfig
  fi

  # Add the CM3 binaries to the user's PATH
  save_env_var PATH "\$PATH:$CM3_HOME/bin"

  if ! host_is_amd64; then
      # CM3: full distribution
      push_d "$CM3_SRC_UNPACK_PATH"
      CM3SRC_TGZ=`download_from_svn "$CM3_SRC_SVNURL"`
      unpack "$CM3SRC_TGZ"
      push_d scripts
      ./do-cm3-core.sh buildship
      ./install-cm3-compiler.sh upgrade
      ./do-cm3-std.sh buildship
      ./do-cm3-std.sh buildship
      pop_d
      pop_d
  fi

  # Fix permissions for CM3_HOME
  sudo chmod -R o-w "$CM3_HOME"
  sudo chown -R root.root "$CM3_HOME"

  if cm3 -version | grep -q "Critical Mass"; then
    status "Successfully compiled and installed CM3."
  else
    error "Failed to install CM3."
    return 1
  fi
}

function install_simplify() {
  if can_run Simplify; then
    # The old Simplify installation under $CM3_HOME is not removed, as
    # it will be replaced anyway.
    if ! (confirm "Simplify seems to be already installed. Reinstall?" &&\
          confirm_rm "$SRC_PATH/$SIMPLIFY_SRC_MAINDIR"); then
      return 0
    fi
  fi

  # Change ownership of CM3_HOME so we can build Simplify without root privileges
  sudo chown -R `id -un`.`id -gn` "$CM3_HOME"

  SIMPLIFY_TBZ2=`download_from_svn "$SIMPLIFY_SRC_SVNURL"`
  push_d "$SRC_PATH"
  unpack "$SIMPLIFY_TBZ2"
  push_d "$SIMPLIFY_SRC_MAINDIR/Simplify"
  for i in cgi-utils digraph prover simplify; do
    push_d "$i"
    cm3
    cm3 -ship
    pop_d
  done
  pop_d
  pop_d

  # Fix permissions in CM3_HOME
  find "$CM3_HOME" -execdir chmod o-w '{}' \;
  sudo chown -R root.root "$CM3_HOME"

  if can_run Simplify; then
    status "Successfully installed Simplify."
  else
    error "Failed to install Simplify."
    return 1
  fi
}

function install_daikon() {
  if can_run `basename "$DAIKON_SH_SVNURL"`; then
    if ! (confirm "Daikon seems to be already installed. Reinstall?" &&\
          confirm_rm "$SRC_PATH/$DAIKON_SRC_MAINDIR"\
            "$BIN_PATH/`basename "$DAIKON_SH_SVNURL"`"\
            "$BIN_PATH/`basename "$DAIKON_OPTIONS_SVNURL"`"); then
      return 0
    fi
    clear_env_var DAIKONDIR
    clear_env_var DAIKONCLASS_SOURCES
  fi

  # Install required packages
  if can_run aptitude; then
    install_deb_packages g++ make
  else
    install_rpm_packages gcc-c++ make
  fi

  # Unpack and patch Daikon
  DAIKON_SRC=`download_from_svn "$DAIKON_SRC_SVNURL"`
  DAIKON_DELTA=`download_from_svn "$DAIKON_DELTA_SVNURL"`
  push_d "$SRC_PATH"
  unpack "$DAIKON_SRC"
  push_d "$DAIKON_SRC_MAINDIR"
  zpatch "$DAIKON_DELTA"
  pop_d
  pop_d

  # Add related environment variables
  save_env_var DAIKONDIR "$SRC_PATH/$DAIKON_SRC_MAINDIR"
  save_env_var DAIKONCLASS_SOURCES 1
  add_profile_line "source \"$DAIKONDIR/bin/daikon.bashrc\""

  # Compile using as many cores as we can
  push_d "$DAIKON_CLASSPATH"
  mcore_make daikon
  pop_d

  # Install Daikon options
  DAIKON_OPTIONS=`download_from_svn "$DAIKON_OPTIONS_SVNURL"`
  cp "$DAIKON_OPTIONS" "${BIN_PATH}"
  DAIKON_OPTIONS_PATH=${BIN_PATH}/`basename "$DAIKON_OPTIONS"`

  # Install Daikon launcher script
  DAIKON_SH=`download_from_svn "$DAIKON_SH_SVNURL"`
  DAIKON_SH_BASENAME=`basename "$DAIKON_SH"`
  chmod +x "$DAIKON_SH"
  sed -r -i \
    -e "/^DAIKON=/s#.*#DAIKON=$DAIKON_CLASSPATH#" \
    -e "/^DAIKON_OPTS_FILE=/s#.*#DAIKON_OPTS_FILE=$DAIKON_OPTIONS_PATH#" \
    "$DAIKON_SH"
  install_script "$DAIKON_SH"

  if "$DAIKON_SH_BASENAME" | fgrep -q "No .decls"; then
    status "Successfully compiled and installed Daikon."
  else
    error "Failed to install Daikon."
    return 1
  fi
}

function install_takuan() {
  if test -d "$TAKUAN_HOME"; then
    if ! (confirm "Takuan seems to be already installed at $TAKUAN_HOME. Reinstall?" &&\
          confirm_rm "$TAKUAN_HOME"); then
      return 0
    fi
    clear_env_var TAKUAN_HOME
  fi

  # We need the Graph and Parallel::ForkManager CPAN modules
  if can_run aptitude; then
    install_deb_packages libgraph-perl libparallel-forkmanager-perl
  else
    install_rpm_packages make gcc-c++
    install_from_cpan YAML Graph Parallel::ForkManager
  fi

  # Set the path to Takuan
  save_env_var TAKUAN_HOME "$TAKUAN_HOME"

  # Checkout the subfolders of the SVN repository that we need
  push_d "$TAKUAN_HOME"
  for d in $TAKUAN_SVNURLS; do
    checkout_from_svn "$d"
  done
  pop_d

  # Compile the instrumenter
  push_d "$TAKUAN_INSTR_SRC_PATH"
  ant -f standalone-build.xml dist
  pop_d

  # Test that the analyzer works
  "$ACTIVEBPEL_LAUNCHER_BASENAME" start
  push_d "$TAKUAN_SAMPLE_PATH"
  if ant; then
    "$ACTIVEBPEL_LAUNCHER_BASENAME" stop
    pop_d
    status "Successfully installed Takuan in $TAKUAN_HOME."
  else
    "$ACTIVEBPEL_LAUNCHER_BASENAME" stop
    pop_d
    error "Failed to install Takuan in $TAKUAN_HOME."
    return 1
  fi
}

function install_takuan_servlet() {
    if test -s "$TAKUAN_SERVLET_WAR"; then
        if ! (confirm "The Takuan servlet seems to be already installed. Reinstall?" &&\
              confirm_rm "$TAKUAN_SERVLET_WAR"); then
            return 0
        fi
    fi

    if can_run aptitude; then
      install_deb_packages libservlet2.4-java
    else
      install_rpm_packages geronimo-servlet-2_4-api geronimo-jsp-2_0-api
      sudo ln -s /usr/share/java/servlet.jar /usr/share/java/servlet-api.jar
      sudo ln -s /usr/share/java/jsp.jar /usr/share/java/jsp-api.jar
    fi

    # Compile and deploy
    push_d "$TAKUAN_SERVLET_SRC"
    if ant -f "${TAKUAN_SERVLET_ANT}" clean local-deploy; then
        # Restart ActiveBPEL
        "$ACTIVEBPEL_LAUNCHER_BASENAME" restart
        status "Successfully installed the Takuan servlet in $TAKUAN_SERVLET_WAR."
        pop_d
    else
        error "Failed to install the Takuan servlet in $TAKUAN_SERVLET_WAR."
        pop_d
        return 1
    fi
}

### GAMERA DEPENDENCIES ########################################################

function install_junit4() {
    JUNIT4_BASENAME=`basename "$JUNIT4_BIN_SVNURL"`
    JUNIT4_DEST="$BIN_PATH/$JUNIT4_BASENAME"
    if test -f "$JUNIT4_DEST"; then
        if ! (confirm "JUnit 4 seems to be already installed. Reinstall?" &&\
              confirm_rm -f "$JUNIT4_DEST")
        then
            return 0
        fi
    fi

    JUNIT4_JAR=`download_from_svn "$JUNIT4_BIN_SVNURL"`
    cp "$JUNIT4_JAR" "$JUNIT4_DEST"

    if (java -cp "$JUNIT4_DEST" junit.textui.TestRunner &>/dev/null; (( $? == 2 ))); then
        status "Successfully installed JUnit 4 in $BIN_PATH"
    else
        error "Failed to install JUnit 4 in $BIN_PATH"
        return 1
    fi
}

function install_mutationanalysis() {
    MTOOL_BASENAME=`basename "$MUTATIONTOOL_SCRIPT_SVNURL"`
    MTOOL_JAR_BASENAME=`basename "$MUTATIONTOOL_JAR_SVNURL"`
    if can_run "$MTOOL_BASENAME"; then
        if ! (confirm "$MTOOL_BASENAME seems to be already installed. Reinstall?" &&\
              confirm_rm "$BIN_PATH/$MTOOL_BASENAME" "$BIN_PATH/$MTOOL_JAR_BASENAME")
        then
            return 0
        fi
    fi

    MTOOL_SCRIPT=`download_from_svn "$MUTATIONTOOL_SCRIPT_SVNURL"`
    MTOOL_JAR=`download_from_svn "$MUTATIONTOOL_JAR_SVNURL"`
    install_script "$MTOOL_SCRIPT"
    cp "$MTOOL_JAR" "$BIN_PATH"

    if ("$MTOOL_BASENAME"; (( $? == 1 ))); then
        status "Successfully installed $MTOOL_BASENAME in $BIN_PATH"
    else
        error "Failed to install $MTOOL_BASENAME in $BIN_PATH"
        return 1
    fi
}

function install_galib() {
    if has_library ga; then
        if ! (confirm "galib seems to be already installed. Reinstall?" &&\
              confirm_rm "$SRC_PATH/$GALIB_SOURCE_MAINDIR")
        then
            return 0
        fi
    fi

    if can_run aptitude; then
      install_deb_packages make g++
    else
      install_rpm_packages make gcc-c++
    fi

    GALIB_SOURCETGZ=`download_from_svn "$GALIB_SOURCETGZ_SVNURL"`
    push_d "$SRC_PATH"
    unpack "$GALIB_SOURCETGZ"
    push_d "$GALIB_SOURCE_MAINDIR"
    # Parallel compilation breaks some builds (libga.a is built after it is
    # first needed). It's probably some mistake in the GAlib Makefile. It's
    # not too big anyway, so there's no harm in using a single core.
    make test
    sudo make install
    pop_d
    pop_d

    if has_library ga; then
        status "galib was sucessfully installed."
    else
        error "Failed to install galib."
        return 1
    fi
}

function install_gamera() {
    if can_run "$GAMERA_EXE"; then
        if ! (confirm "GAmera seems to be already installed. Reinstall?" &&\
              confirm_rm "$GAMERA_HOME" "$BIN_PATH/$GAMERA_EXE")
        then
            return 0
        fi
    fi

    checkout_from_svn "$GAMERA_SOURCE_SVNURL" "$GAMERA_HOME"
    push_d "$GAMERA_HOME"
    mcore_make
    ln -s "`pwd`/$GAMERA_EXE" "$BIN_PATH/$GAMERA_EXE"
    pop_d

    if can_run "$GAMERA_EXE"; then
        status "GAmera has been successfully installed."
    else
        error "Failed to install GAmera."
        return 1
    fi
}

function install_koura() {
    if can_run "$KOURA_LAUNCHER_BASENAME"; then
        if ! (confirm "Koura seems to be already installed. Reinstall?" &&\
              confirm_rm "$GAMERA_HOME/$KOURA_EXE"\
                "$BIN_PATH/$KOURA_LAUNCHER_BASENAME"\
                $KOURA_EXTRADEPS_SVNURLS)
        then
            return 0
        fi
    fi

    # Checkout the source, compile it and place it into GAmera's directory
    KOURA_SOURCE_MAINDIR=`basename "$KOURA_SOURCE_SVNURL"`
    if can_run aptitude; then
      install_deb_packages monodevelop
    else
      # openSUSE 11.0 has a pretty old version: mdtool doesn't
      # include support for the -c and -t flags for specifying the
      # configuration and target, respectively.
      install_rpm_packages monodevelop
    fi
    push_d "$SRC_PATH"
    checkout_from_svn "$KOURA_SOURCE_SVNURL"
    push_d "$KOURA_SOURCE_MAINDIR"
    mdtool build
    KOURA_EXE_PATH="$GAMERA_HOME/$KOURA_EXE"
    cp "bin/$KOURA_CONFIGURATION/$KOURA_EXE" "$KOURA_EXE_PATH"
    pop_d
    pop_d

    # openSUSE 11.0: Mono binaries can't be run as regular executables,
    # unlike in Ubuntu 9.04 and 9.10, so a simple symbolic link won't do.
    # We'll have to place a small wrapper script in $BIN_PATH.
    KOURA_LAUNCHER_PATH="$BIN_PATH/$KOURA_LAUNCHER_BASENAME"
    cat > "$KOURA_LAUNCHER_PATH" <<EOF
#!/bin/sh
mono "$KOURA_EXE_PATH"
EOF
    chmod +x "$KOURA_LAUNCHER_PATH"

    # Install the rest of the dependencies (img/ and Perl scripts)
    push_d "$GAMERA_HOME"
    for i in $KOURA_EXTRADEPS_SVNURLS; do
        cp -r "`download_from_svn "$i"`" .
    done
    pop_d

    if can_run mono && can_run "$KOURA_LAUNCHER_BASENAME"; then
        status "Successfully installed Koura."
    else
        error "Failed to install Koura."
        return 1
    fi
}

### MAIN SCRIPT BODY ##########################################################

if [ `whoami` = "root" ]; then
  echo "This script should not be run as root."
  exit 1
fi

if [[ "$#" != 1 ||\
      ( "$1" != "both" && "$1" != "takuan" && "$1" != "gamera" ) ]]; then
  if [ -z "$TARGET" ]; then
    # No default target was defined, and the user did not provide any,
    # or provided a wrong one: print usage and exit.
    echo "Usage: $0 (both|takuan|gamera)"
    exit 1
  fi
else
  TARGET="$1"
fi

status "Installing common requirements..."
prepare_repositories

# Load the latest .profile, but careful: some of its commands may
# fail. For instance, the default openSUSE 11.1 per-user profile file
# checks whether $PROFILEREAD is unset. If it is set, the check will
# fail and with "set -e" still in effect, we would bail out of the
# whole install process. It's better to switch to "set +e" temporarily
# while loading the .profile file.
set +e
source ~/.profile
set -e

install_base_deps
install_java6_jdk
install_tomcat5
install_activebpel
install_xpath_monitoring
install_xmlbeans
install_bpelunit
install_saxon

if [[ "$TARGET" == "takuan" || "$TARGET" == "both" ]]; then
    status "Installing Takuan..."
    install_cm3
    install_simplify
    install_daikon
    install_takuan
    install_takuan_servlet
fi

if [[ "$TARGET" == "gamera" || "$TARGET" == "both" ]]; then
    status "Installing GAmera..."
    install_junit4
    install_mutationanalysis
    install_galib
    install_gamera
    install_koura
fi

status "Done. Please run '. ~/.profile' or log out and back in."
